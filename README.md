# REDCap-API-Examples
example API scripts for use with REDCap

## Token Management

The REDCap API utilizes tokens, which contain logon credentials. Each project and individual user will have a unique token. They should be treated like usernames and passwords so they should never be shared between users. 

There are a number of methods for secure storage of API tokens. This walk-through relies on an .Renviron file that is stored in the user's HOME directory, as described in Hadley Wickham's httr package at: https://httr.r-lib.org/articles/secrets.html#locally

### Additional resources

- R Help: `?Startup`
- http://www.onthelambda.com/2014/09/17/fun-with-rprofile-and-customizing-r-startup/
- http://blog.revolutionanalytics.com/2015/11/how-to-store-and-use-authentication-details-with-r.html

### .Renviron

Create a new .Renviron file in the home directory if there isn't one already.

```
if(!file.exists("~/.Renviron")) { # only create if not already there
    file.create("~/.Renviron")    # (don't overwrite it)
}
file.edit("~/.Renviron")
```

add each API token with a unique key name
```
# .Renviron

# REDCap API Tokens
redcap_tbs_test_project = "E653F6B53DCDF44A860D0F6B3EE262ED"
```


## Usage in scripts

### export_to_csv.R
```
#' export_to_csv.R
#'
#' Exports a REDCap project to a .csv file

#' load packages
library(REDCapR)
library(readr)

#' get data from REDCap
uri <- "https://redcap.gamutqi.org/api/"

results <-
        redcap_read(redcap_uri = uri,
                    token = Sys.getenv("redcap_tbs_test_project")
                    )

mydata <- results$data

#' save to file
readr::write_csv(mydata, "Data/mydata.csv")


```


### import_from_csv.R
```
#' import_from_csv.R
#'
#' imports a .csv file to a REDCap project

#' load packages
library(REDCapR)
library(readr)

#' get data from .csv file
mydata <-
        read_csv("Data/mydata.csv")

#' send to REDCap
uri <- "https://redcap.gamutqi.org/api/"

results <-
        redcap_write(ds_to_write = mydata,
                     redcap_uri = uri,
                     token = Sys.getenv("redcap_tbs_test_project")
                     )
```

### ETL_to_SQL.R
```
#' ETL_to_SQL.R
#'
#' imports a .csv file to a REDCap project#' load packages
library(REDCapR)
library(tidyverse)

#' get data from REDCap
uri <- "https://redcap.gamutqi.org/api/"

results <-
  redcap_read(redcap_uri = uri,
              token = Sys.getenv("redcap_tbs_test_project")
  )

mydata <- results$data

#' send to database
#' requires the specified DSN
con <- DBI::dbConnect(odbc::odbc(), 
                      dsn = "EDWDBDev", 
                      database = "Epic_Work"
                      #` need to add uid/pwd parameters for AVA (Linux)
                      #' uid = Sys.getenv("DBUSERNAME"), pwd = Sys.getenv("DBPASSWORD")
) 

#' DBI::dbBegin(con)
DBI::dbWriteTable(con, "tbs_test", mydata, overwrite = TRUE)
#' DBI::dbReadTable(con, "tbs_test")
#' DBI::dbCommit(con)
#' DBI::dbRollback(con)
DBI::dbDisconnect(con)

```
