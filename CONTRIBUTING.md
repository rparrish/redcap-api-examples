This file contains guidelines that should be followed when making any
changes to the repository. All text before the first level 1 (L1) header
will be ignored by the Pull request guidelines add-on.

# Data files

All data files (.csv, .Rdata, .xlsx, etc. ) should be saved in one location under the /Data folder. 

## Move data files to /Data

The /Data folder should be in .gitignore, which will prevent them from being added to the repository. This ensures that data files with PHI and PII are not inadvertently shared.

## Add /Data to .gitignore


## Get buy-in from your team (L2 header for guidelines)

These guidelines don't mean much if a lot of people in your team disagree
with them. So if you're making a change to the CONTIRBUTING.md file, make
sure to use get the change reviewed by members of your team who are
subject-matter experts on the topic using a pull request.

While it may be impossible to get everybody to agree to everything, make
sure that most people agree with the guidelines at all times.

## Good guidelines are specific and measurable

It is really hard to follow vague guidelines. For example "Write pretty
Python code" is a very vague guideline and could easily spark disagreement
between the author and the reviewers of the pull request. On the other
hand, "Write PEP8 compliant Python code" is a much better guideline. It
specifies exactly what is required and how to test whether the guideline
is being followed.

# Yet another topic (add as many topics as you need)

## You get the idea

Now go install the add-on and create your own CONTRIBUTING.md file.

# PHS Git Commit guidelines

## No PHI or PII data

## No Tokens or passwords
